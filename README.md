# AscensionApi

[![build status](https://gitlab.com/wsharp07/ascension-api/badges/master/build.svg)](https://gitlab.com/wsharp07/ascension-api/commits/master)

To start your Phoenix app:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Start Phoenix endpoint with `mix phoenix.server`

API is hosted at [`localhost:4001`](http://localhost:4001) from your browser.